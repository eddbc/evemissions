<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array(
        'as' => 'home',
        'uses' => 'PagesController@index'
    )
);

Route::get('/login', array(
        'as' => 'login',
        'uses' => 'AuthController@login'
    )
);
Route::get('/logout', array(
        'as' => 'logout',
        'uses' => 'AuthController@logout'
    )
);

Route::post('/user/{id}/missions', 'UserMissionController@store');

Route::resource('users', 'UsersController');
Route::resource('missions', 'MissionsController');
Route::resource('categories', 'CategoriesController');
Route::resource('areas', 'AreasController');
