<?php

class AreasController extends \BaseController
{

	/**
	 * Display a listing of areas
	 *
	 * @return Response
	 */
	public function index()
	{
		$areas = Area::all();

		return View::make('areas.index', compact('areas'));
	}

	/**
	 * Show the form for creating a new area
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('areas.create');
	}

	/**
	 * Store a newly created area in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Area::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Area::create($data);

		return Redirect::route('areas.index');
	}

	/**
	 * Display the specified area.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{
		$area = Area::findOrFail($id);

		return View::make('areas.show', compact('area'));
	}

	/**
	 * Show the form for editing the specified area.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$area = Area::find($id);

		return View::make('areas.edit', compact('area'));
	}

	/**
	 * Update the specified area in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id)
	{
		$area = Area::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Area::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$area->update($data);

		return Redirect::route('areas.index');
	}

	/**
	 * Remove the specified area from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Area::destroy($id);

		return Redirect::route('areas.index');
	}

}
