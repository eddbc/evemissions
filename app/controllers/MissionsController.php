<?php

use EveMissions\Services\MissionCreatorService;

class MissionsController extends \BaseController
{
	protected $missionCreator;

	function __construct(MissionCreatorService $missionCreator)
	{
		$this->missionCreator = $missionCreator;
	}


	/**
	 * Display a listing of missions
	 *
	 * @return Response
	 */
	public function index()
	{
		$missions = Mission::all();

		return View::make('missions.index', compact('missions'));
	}

	/**
	 * Show the form for creating a new mission
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('missions.create');
	}

	/**
	 * Store a newly created mission in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		try {
			$this->missionCreator->make(Input::all());
		} catch (\EveMissions\Validators\ValidationException $e) {
			return Redirect::back()->withInput()->withErrors($e->getErrors());
		}

		return Redirect::route('missions.index');
	}

	/**
	 * Display the specified mission.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{
		$mission = Mission::findOrFail($id);

		return View::make('missions.show', compact('mission'));
	}

	/**
	 * Show the form for editing the specified mission.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$mission = Mission::find($id);

		return View::make('missions.edit', compact('mission'));
	}

	/**
	 * Update the specified mission in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id)
	{
		$mission = Mission::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Mission::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$mission->update($data);

		return Redirect::route('missions.index');
	}

	/**
	 * Remove the specified mission from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Mission::destroy($id);

		return Redirect::route('missions.index');
	}

}
