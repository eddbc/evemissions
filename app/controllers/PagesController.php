<?php

class PagesController extends BaseController
{

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		$users = User::all();
		$missions = Mission::all();
		$categories = Category::all();
		$areas = Area::all();

		return View::make('front-page', array(
			'users' => $users,
			'missions' => $missions,
			'categories' => $categories,
			'areas' => $areas,
		));
	}

	public function oldIndex()
	{
		return View::make('hello');
	}
}
