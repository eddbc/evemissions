<?php

class AuthController extends \BaseController {

	/**
	 * GET /login
	 *
	 * @return Response
	 */
	public function login()
	{
		$user = User::all()->random();
		Auth::login($user);
		return Redirect::back();
	}

	/**
	 * GET /logout
	 *
	 * @return Response
	 */
	public function logout() {
		Auth::logout();
		return Redirect::back();
	}
}