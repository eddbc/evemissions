<?php

class AreasTableSeeder extends Seeder {

	public function run()
	{

        $names = array(
            'High-Sec',
            'Low-Sec',
            'NPC Null-Sec',
            'Null-Sec',
            'Any'
        );

		foreach($names as $name)
		{
			Area::create([
                'name' => $name
			]);
		}
	}

}