<?php

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
        $names = array(
            'Combat',
            'Trading',
            'Exploration',
            'Ship Fitting',
            'Social'
        );

        foreach($names as $name)
        {
            Category::create([
                'name' => $name
            ]);
        }
	}

}
