<?php

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			User::create([
                'id' => $index,
                'name' => $faker->name,
                'corporation' => (12340+$index)
			]);
		}
	}

}
