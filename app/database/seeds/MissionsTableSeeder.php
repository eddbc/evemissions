<?php

use Faker\Factory as Faker;

class MissionsTableSeeder extends Seeder
{

	public function run()
	{
		$faker = Faker::create();

		foreach (range(1, 10) as $index) {
			/** @var Area $area */
			$area = Area::all()->random();
			/** @var Category $category */
			$category = Category::all()->random();
			/** @var User $user */
			$user = User::all()->random();

			Mission::create([
				'title' => $faker->sentence(),
				'description' => $faker->paragraph(),
				'area_id' => $area->id,
				'category_id' => $category->id,
				'rewardIsk' => 1000000,
				'available' => 100,
				'expiry' => 100,
				'creatorUser' => $user->id,
				'official' => true,
				'maxAge' => 100
			]);
		}
	}
}
