<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserMissionTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_mission', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->integer('mission_id')->unsigned()->index();
			$table->integer('state')->unsigned();
			$table->string('screenshot_url');

			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('mission_id')->references('id')->on('missions')->onDelete('cascade');
			$table->foreign('state')->references('id')->on('states')->onDelete('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_mission');
	}

}
