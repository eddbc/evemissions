<?php

/**
 * Tag
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mission[] $missions
 * @method static \Illuminate\Database\Query\Builder|\Tag whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Tag whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Tag whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Tag whereUpdatedAt($value)
 */
class Tag extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

    /**
     * Returns all missions with this tag
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function missions() {
        return $this->belongsToMany('Mission');
    }

}