<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * User
 * 
 * Users are authenticated via EVE Online SSO(Single Sign-On). Users complete missions in return for some reward
 *
 * @property integer $id
 * @property string $name
 * @property integer $corporation
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mission[] $missions
 * @method static \Illuminate\Database\Query\Builder|\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereCorperation($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Role[] $roles
 * @method static \Illuminate\Database\Query\Builder|\User whereCorporation($value)
 * @property string $remember_token
 * @method static \Illuminate\Database\Query\Builder|\User whereRememberToken($value) 
 */
class User extends Model implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

    static $rules = ['', ''];


    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    /**
     * Returns the missions the user has accepted
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function missions() {
        return $this->belongsToMany('Mission', 'user_mission');
    }

    /**
     * Returns all the roles this user has
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles() {
        return $this->belongsToMany('Role', 'user_role');
    }

    public function getLink($text)
    {
        return HTML::linkRoute('users.show', $text, array($this->id));
    }
}
