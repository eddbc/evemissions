<?php
/**
 * Created by PhpStorm.
 * User: edward
 * Date: 10/12/2014
 * Time: 14:45
 */

abstract class Model extends \Eloquent {
    public function getLink($text) {
        return HTML::linkRoute('home',$text);
    }
} 