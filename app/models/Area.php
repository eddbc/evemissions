<?php

/**
 * State
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\State whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereUpdatedAt($value)
 */
class Area extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

    public function getLink($text)
    {
        return HTML::linkRoute('areas.show', $text, array($this->id));
    }
}