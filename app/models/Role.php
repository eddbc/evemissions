<?php

/**
 * Role
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Role whereUpdatedAt($value)
 */
class Role extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

    /**
     * Returns all the users with this role
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
        return $this->belongsToMany('User', 'user_role');
    }
}
