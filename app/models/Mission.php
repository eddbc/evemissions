<?php

/**
 * Mission
 * 
 * Missions are posted for users to accept and complete for some reward
 *
 * @property integer $id
 * @property string $title
 * @property integer $area
 * @property integer $category
 * @property string $description
 * @property integer $payout
 * @property integer $available
 * @property integer $expiry
 * @property integer $creatorUser
 * @property boolean $official
 * @property integer $maxAge
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\Mission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereArea($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission wherePayout($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereAvailable($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereExpiry($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereCreatorUser($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereOfficial($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereMaxAge($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Mission whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Tag[] $tags
 * @property integer $area_id
 * @property integer $category_id
 * @property integer $rewardIsk
 * @property-read \User $creator
 * @method static \Illuminate\Database\Query\Builder|\Mission whereAreaId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Mission whereCategoryId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Mission whereRewardIsk($value) 
 */
class Mission extends Model
{

	// Add your validation rules here
	public static $rules = [
		'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

	/**
	 * Returns all users that have accepted the mission
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function users()
	{
		return $this->belongsToMany('User', 'user_mission');
	}

	/**
	 * Returns all tags for this mission
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function tags()
	{
		return $this->belongsToMany('Tag');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Area
	 */
	public function area()
	{
		return $this->belongsTo('Area');
	}

	public function category()
	{
		return $this->belongsTo('Category');
	}

	public function creator()
	{
		return $this->belongsTo('User', 'creatorUser');
	}

	public function getLink($text)
	{
		return HTML::linkRoute('missions.show', $text, array($this->id));
	}
}