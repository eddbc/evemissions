<div class="panel panel-default">
	<div class="panel-heading">Users</div>
	<table class="table">
		<thead>
		<tr>
			<th>Name</th>
			<th>Missions Accepted</th>
			<th>Missions Completed</th>
			<th>ISK Earned</th>
		</tr>
		</thead>
		<tbody>
		@foreach($users as $user)
			<tr>
				<td>{{ $user->name }}</td>
				<td>0</td>
				<td>0</td>
				<td>0 ISK</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>