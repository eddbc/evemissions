<div class="panel panel-default">
	<div class="panel-heading">
	    @yield('panel.title')
	        <h3 class="panel-title">{{{ $title }}}</h3>
	</div>
	<div class="panel-body">
	    @yield('panel.content')
	<ul>
		@foreach($items as $item)
			@if(isset($property))
				<li>{{ $item->$property }}</li>
			@elseif(isset($method))
				<li>{{ $item->$method() }}</li>
			@else
				<li>{{{ $item }}}</li>
			@endif
		@endforeach
	</ul>
	</div>
</div>