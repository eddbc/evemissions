<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			{{ link_to_route('home', 'EVE Missions', null, ['class' => 'navbar-brand']) }}
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li>
					{{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">Current Missions</a>--}}
					{{--<ul class="dropdown-menu">--}}
					{{--<li><a href="#">Top Viewed</a>--}}
					{{--</li>--}}
					{{--<li><a href="#">Top Rated</a>--}}
					{{--</li>--}}
					{{--<li><a href="#">Newest</a>--}}
					{{--</li>--}}
					{{--<li><a href="#">Staff Picks</a>--}}
					{{--</li>--}}
					{{--</ul>--}}
					<a href="/missions">Available Missions</a>
				</li>
				@if(!Auth::guest())
					<li>
						<a href="/missions/create">Submit a Mission</a>
					</li>
				@endif
				<li><a href="#">Help</a>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				@if (Auth::guest())
					<li><a href="/login" class="eve-sso-button"><img src="/images/EVE_SSO_Login_Buttons_Small_Black.png"></a></li>
				@else
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }}<span
									class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li><a href="/logout">Logout</a></li>
						</ul>
					</li>
				@endif
			</ul>
		</div>
	</div>
</nav>