<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Eve Missions</title>

	<!-- Stylesheets -->
	<link href="/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="/css/style.css" rel="stylesheet" type="text/css">

</head>
<body>

@include('layouts.partials.navbar')
<div class="container">
	<div class="content">
		@include('layouts.partials.errors')
		@yield('content')
	</div>
</div>

<!-- Javascript -->

{{--<script src="/js/jquery/jquery.js"></script>--}}
<script src="/js/jquery/jquery.min.js"></script>
<script src="/js/bootstrap/bootstrap.js"></script>
</body>
</html>