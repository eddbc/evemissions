@extends('layouts.default')

@section('content')

	<h1>Create Mission</h1>

	{{ Form::open(['url' => '/missions', 'class' => 'form']) }}

	<div class="form-group">
		{{ Form::label('title', 'Title') }}
		{{ Form::text('title', null, ['class' => 'form-control']) }}
	</div>

	<div class="form-group">
		{{ Form::label('description', 'Description') }}
		{{ Form::textarea('description', null, ['class' => 'form-control']) }}
	</div>

	<div class="form-group">
		{{ Form::label('area', 'Area') }}
		{{ Form::text('area', null, ['class' => 'form-control']) }}
	</div>

	<div class="form-group">
		{{ Form::label('category', 'Category') }}
		{{ Form::text('title', null, ['class' => 'form-control']) }}
	</div>

	<div class="form-group">
		{{ Form::label('reward-isk', 'ISK Reward') }}
		{{ Form::text('title', null, ['class' => 'form-control']) }}
	</div>

	<div class="form-group">
		{{ Form::submit('Create Mission', ['class' => 'btn btn-primary']) }}
	</div>

	{{ Form::close() }}

@stop