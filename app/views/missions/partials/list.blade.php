<div class="panel panel-default">
	<div class="panel-heading">Available Missions</div>
	<table class="table">
		<thead>
		<tr>
			<th>Name</th>
			<th>Description</th>
			<th>Category</th>
			<th>Reward</th>
			<th>Area</th>
		</tr>
		</thead>
		<tbody>
		@foreach($missions as $mission)
			<tr>
				<td>{{ $mission->getLink($mission->title) }}</td>
				<td>{{{ $mission->description }}}</td>
				<td>{{{ $mission->category->name }}}</td>
				<td>{{{ $mission->rewardIsk }}} ISK</td>
				<td>{{{ $mission->area->name }}}</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>