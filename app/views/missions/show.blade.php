@extends('layouts.default')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">{{{ $mission->title }}}</h3>
		</div>
		<div class="panel-body">
			<p>Description : {{{$mission->description }}}</p>

			<p>Area : {{{$mission->area->name }}}</p>

			<p>Category : {{{$mission->category->name }}}</p>

			<p>Reward : {{{$mission->reward }}} ISK</p>

			<p>Made By : {{{$mission->creator->name }}}</p>

			@if( !Auth::guest())
				{{ Form::open(['url' => 'user/'.Auth::user()->id.'/missions', 'class' => 'form']) }}
				<div class="form-group">
					{{ Form::hidden('mission_id', $mission->id) }}
					{{ Form::submit('Accept Mission', ['class' => 'btn btn-primary']) }}
				</div>

				{{Form::close()}}
			@endif
		</div>


	</div>

@stop