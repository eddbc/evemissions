<?php
/**
 * Created by PhpStorm.
 * User: edward
 * Date: 12/12/2014
 * Time: 17:10
 */

namespace EveMissions\Services;

use EveMissions\Validators\MissionValidator;
use EveMissions\Validators\ValidationException;
use Mission;

class MissionCreatorService {

	/**
	 * @var MissionValidator
	 */
	protected $validator;

	function __construct(MissionValidator $missionValidator)
	{
		$this->validator = $missionValidator;
	}


	public function make(array $attributes) {
		if ($this->validator->isValid($attributes)) {
			// TODO Create mission more gooder
			Mission::create($attributes);
		} else {
			throw new ValidationException('Mission validation failed', $this->validator->getErrors());
		}
	}
}