<?php
/**
 * Created by PhpStorm.
 * User: edd
 * Date: 13/12/2014
 * Time: 00:05
 */

namespace EveMissions\Validators;


use Exception;

class ValidationException extends \Exception {

	protected $errors;

	function __construct($message = null, $errors, $code = 0, Exception $previous = null)
	{
		parent::__construct($message = null, $code = 0, $previous = null);

		$this->$errors = $errors;
	}

	public function getErrors() {
		return $this->errors;
	}
}