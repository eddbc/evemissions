<?php
/**
 * Created by PhpStorm.
 * User: edward
 * Date: 12/12/2014
 * Time: 17:00
 */

namespace EveMissions\Validators;

use Validator as V;

abstract class Validator
{
	protected static $rules;
	protected $errors;

	public function getErrors()
	{
		return $this->errors;
	}

	public function isValid(array $attributes)
	{
		$validator = V::make($attributes, static::$rules);

		if ($validator->fails()) {
			$this->errors = $validator->messages();
			return false;
		}

		return true;
	}
}
