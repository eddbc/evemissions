# **Eve-Missions** #

Built using the Laravel Framework.

## Getting started ##

You can run Laravel in a Vagrant virtual machine, or directly on your computer for development purposes. I recommend using Vagrant, but if you don't want to go through the hassle of setting that up, you can fall back to the local machine

### Vagrant ###
1. The recommended vagrant box for developing this project is Laravel Homestead. Detailed setup instructions can be found [here](http://laravel.com/docs/4.2/homestead)
2. Once your vagrant box is setup and running, you should run the provisioning script found at evemissions/provision/setup.sh (Not currently implemented. Soon tm). This will set up the required database.
3. Next you must install the required dependencies. To get the php dependencies run ```composer install``` in the project directory.
4. Copy <project root>/bootstrap/environment.example.php as environment.php. Copy <project root>/example.env.local.php as .env.local.php.
5. Set up the database tables by running ```php artisan db:migrate```, and populate with mock data with ```php artisan db:seed```
6. You should now be able to access the project with your web browser at the address you set up in Homestead (eg. ```eve.app:8000```)

### Local Machine ###
1. You can also run the project on your local machine. First, you will need to have PHP and MySQL(or MariaDB) installed.
2. You will need to create a database called ```evemissions```, and a user:password of homestead:secret
3. To install PHP dependencies, composer mush be installed
4. Next you must install the required dependencies. To get the php dependencies run ```composer install``` in the project directory.
5. Copy <project root>/bootstrap/environment.example.php as environment.php. Copy <project root>/example.env.local.php as .env.local.php.
6. Set up the database tables by running ```php artisan db:migrate```, and populate with mock data with ```php artisan db:seed```
7. You should now be able to launch the app with ```php artisan serve``` in the project root. This will allow you to access the project in your web browser at ```localhost:8000```
