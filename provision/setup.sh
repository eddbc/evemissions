#!/bin/sh
# Custom Project Provisioning

echo "Creating additional database(s) for project..."
sudo mysql -u homestead -psecret --execute "CREATE DATABASE evemissions; " homestead
echo "Application databases created."

echo "Flushing privileges..."
sudo mysql -u homestead -psecret --execute "FLUSH PRIVILEGES;" homestead
echo "Flushed privileges."

sudo service mysql stop
sudo service mysql start
